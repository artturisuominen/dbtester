/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agentit.dbtester;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.ini4j.Wini;

/**
 * @author Artturi
 */
public class DBTester {
    static Logger log = Logger.getLogger(DBTester.class.getName());
    static Logger errorLog = Logger.getLogger("DBTesterError");
    static int interval;
    static String url;
    static String user;
    static String pass;
    
    
    public static void main(String[] args) {
        try{
            log.info("Initializing settings");
            Wini ini = new Wini(new File("./DBTester.ini"));
            interval = Integer.parseInt(ini.fetch("DBTester", "interval")) * 1000; //Interval in ms
            url = ini.fetch("DBTester", "url");
            user = ini.fetch("DBTester", "user");
            pass = ini.fetch("DBTester", "pass");
            log.info("Got settings: \n interval : " + interval + "\n url : " + url + "\n user : " + user + "\n pass : " + pass);
        } catch (IOException ex) {
            errorLog.error("IOException: " + ex);
            //errorLog.error("Using default interval (60 sec)");
            //interval = 60 * 1000;
            errorLog.error("Shutting down");
            System.exit(0);
        }
            do {
            try {
                
                log.debug("Test log");
                errorLog.debug("Test log (Error)");
                log.info("Getting connection..");
                Connection conn = DriverManager.getConnection(url, user, pass);
                log.info("Preparing stmt..");
                PreparedStatement stmt = conn.prepareStatement("SELECT COUNT(*) as count FROM Messages ");
                log.info("Executing query..");
                ResultSet rs = stmt.executeQuery();
                int count = rs.getInt("count");
                log.info("count : " + count);
                rs.close();
                conn.close();
                log.info("Succesful query, sleeping for " + interval + " seconds..");
                Thread.sleep(interval);
            } catch (SQLException ex) {
                try {
                    errorLog.error("SQLException: " + ex + "\n Sleeping for " + interval + " seconds");
                    Thread.sleep(interval);
                } catch (InterruptedException ex1) {
                    errorLog.error("Exception: " + ex1);
                }
            } catch (Exception ex) {
                try {
                    errorLog.error("Exception: " + ex + "\n Sleeping for " + interval + "seconds");
                    Thread.sleep(interval);
                } catch (InterruptedException ex1) {
                    errorLog.error("Exception: " + ex1);
                }
                
            }
        } while (true);
    }
}
